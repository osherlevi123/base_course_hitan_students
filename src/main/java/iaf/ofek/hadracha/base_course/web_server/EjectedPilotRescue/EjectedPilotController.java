package iaf.ofek.hadracha.base_course.web_server.EjectedPilotRescue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("ejectedPilotRescue/")
public class EjectedPilotController {


    @Autowired
    private EjectionsImporter database;

    @Autowired
    private AirplanesAllocationManager manager;

    @GetMapping("infos")
    public List<EjectedPilotInfo> getPilots() {
        return database.getDataBase().getAllOfType(EjectedPilotInfo.class);
    }

    @GetMapping("takeResponsibility")
    public void assignRescuer (@RequestParam int ejectionId, @CookieValue("client-id") String clientId) {
        EjectedPilotInfo info = database.getDataBase().getByID(ejectionId,EjectedPilotInfo.class);
        if (info.getAllocatedAirplanes().isEmpty()) {
            info.setRescuedBy(clientId);
            manager.allocateAirplanesForEjection(info,clientId);
            database.getDataBase().update(info);

        }
    }


}
